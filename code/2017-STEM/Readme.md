The Pew 2017 STEM Survey
========================

The gender responses for those who identified with each of the "teaching" occupations.

![Gender breakdown](../../images/2017-STEM-gender-1.png "Gender breakdown")
