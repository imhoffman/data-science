import matplotlib.pyplot as plt
import numpy as np
import pyreadstat

##  https://github.com/Roche/pyreadstat
#pew, meta = pyreadstat.read_sav('materials for public release/2017 Pew Research Center STEM survey.sav', user_missing=True, apply_value_formats=True)
pew, meta = pyreadstat.read_sav('../../data/2017-STEM/materials for public release/2017 Pew Research Center STEM survey.sav', user_missing=True )
pew_labels = pyreadstat.set_value_labels(pew, meta, formats_as_category=True)
##  to have column names be the column labels
#pew.columns = meta.column_labels
##  or simply use the column names from the meta data
pew.columns = meta.column_names

##  diagnostics
#print(pew.head())
#print(meta.missing_ranges)
#print(meta.column_names)
#print(meta.column_labels)
#print(meta.number_rows)
#print(meta.number_columns)
#print(meta.file_label)
#print(meta.file_encoding)

###  pandas fun facts
###  - the following two lines are equivalent
###  pew["PPGENDER"][ pew["OCCUPATION_col"] == 8. ].to_numpy()
###  pew.loc[ [ i for i in range(len(pew)) if pew.loc[i,"OCCUPATION_col"] == 8. ], "PPGENDER" ].to_numpy()
###  - there should be a third way to do it without an explicit 'i'

###
###  data selection and conditioning
###
nf = pew.dropna(subset=["PPGENDER", "OCCUPATION_col","TEACHSTEM","SCH4"])
nf = nf.drop( nf[ (nf.PPGENDER > 2) | (nf.OCCUPATION_col > 28) | (nf.TEACHSTEM > 2) | (nf.SCH4 > 4) ].index )
nf.reset_index(drop=True, inplace=True)
#nf.columns = meta.column_names
print("\n dropped %d rows because of NaN's\n\n" % ( len(pew)-len(nf) ) )


###
###  logistic fitting
###
def sigmoid ( z ):
  return np.divide( 1.0, np.add( 1.0, np.exp( -z ) ) )

def h ( A, B ):
  return sigmoid( np.dot( A, B ) )

##  load up fitting vectors
X = np.array( [ np.ones( len(nf) ),
               nf.OCCUPATION_col.to_numpy(),
               nf.TEACHSTEM.to_numpy(),
               nf.SCH4.to_numpy(),
               ] ).T

##   1's and 2's; not 0's and 1's ...
y = np.array( [ nf["PPGENDER"].to_numpy() ] ).T

m,n = X.shape

##  beta vector of features and its update swapper
beta = np.array( [ np.ones(n) ]).T
temp = np.array( [ np.ones( len(beta) ) ]).T

##  tuning constants
tol = 1.0E-13
alpha = 1.0E-3
max_iter = 30000

##  learning loop
gain = tol + 1.
count = 0
S = 1.
while gain > tol and count < max_iter:
  for i in range( len(beta) ):    # vectorize this loop ...
    temp[i] = beta[i] - alpha*np.sum(
                  np.dot(
                    ( h( X, beta ) - y ).T,
                    X[:,i] ) )
  beta = temp
  count = count + 1
  S = np.dot( ( np.dot( X, beta ) - y ).T, np.dot( X, beta) - y )
  gain = abs( ( gain - S )/gain )
  if count%3000 == 0:
    print("After %6d counts the sum of the squares of the residuals is %.3f" % (count, S))

print("\n %s after %d iterations\n\n" % ("Gave up" if count==max_iter else "Converged",count) )
print(beta)



##
##  plotting
##
xx = np.linspace( np.amin(X,axis=0), np.amax(X,axis=0), num=8192 )
yySCH = sigmoid( beta[0] + xx[:,3]*beta[3] )
yyTEA = sigmoid( beta[0] + xx[:,2]*beta[2] )
fig, (SCH,TEA) = plt.subplots(1, 2, sharey=True, tight_layout=True)

##  first sub plot
SCH.plot( xx[:,3], yySCH, 'b')
SCH.scatter( X[:,3], y, color='r')
SCH.set_title("SCH4")
#A8.set_xticks( [1.25,1.75] )
#A8.set_xticklabels( ['1: Male','2: Female'] )
SCH.set_ylabel('PPGENDER')

##  second sub plot
TEA.plot( xx[:,2], yyTEA, 'b')
TEA.scatter( X[:,2], y, color='r')
TEA.set_title("TEACHSTEM")
#A8.set_xticks( [1.25,1.75] )
#A8.set_xticklabels( ['1: Male','2: Female'] )

#plt.legend()
plt.show()
#plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, frameon=False, metadata=None)


