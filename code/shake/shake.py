import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import sys
sys.path.append('../tools')
from bclearn import logistic
from bclearn.general import *

stuff = pd.read_csv("../../data/shake/detect.dat", sep=r"\s+")

###
###  data selection and conditioning
###
nf = stuff.dropna()
nf.reset_index(drop=True, inplace=True)
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


###
###  logistic fitting
###
##  load up fitting vectors
##   the features/predictors
x_label="$\log_{10}$ of degrees distant"
y_label="magnitude, converted to $M_W$"
X = np.array( [ np.ones( len(nf) ),
               np.log10( nf.iloc[:,1].to_numpy() ),
               nf.iloc[:,2].to_numpy(),
               np.log10( nf.iloc[:,1].to_numpy() )*np.log10( nf.iloc[:,1].to_numpy() ),
               nf.iloc[:,2].to_numpy()*nf.iloc[:,2].to_numpy(),
               ] ).T
boundary = logistic.quadratic_boundary
#boundary = logistic.linear_boundary

##  the response
y = np.array( [ nf.iloc[:,0].to_numpy() ] ).T
pos_response = "detection"
neg_response = "non-detection"

m,n = X.shape

##  standardization
Z, X_mu, X_std = standardizer( X )

##  initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T

##  learning loop
beta = gradient_descent(
         y, Z, beta, logistic.hypothesis, logistic.cost,
         alpha = 0.70E-1, tol = 1.0E-08, max_iter = 300000, report =  8000 )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std )

##
##  plotting
##
plotter ( X, theta, y, boundary,
        Plogit = [ 0.35, 0.50, 0.65, 0.80 ],
        styles = [ ":", "-.", "--", "-" ],
        colors = [ 'b', 'r', 'g' ],
        prediction = logistic.prediction,
        x_label = x_label, y_label = y_label,
        markers = [ '+', 'x' ],
        pos_response = pos_response, neg_response = neg_response,
        show = False, sized = False )

#  second subplot
#Zplt.scatter(
# [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 0 ],
# [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 0 ],
# color='r', marker='x', label=neg_response )
#Zplt.scatter(
# [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 1 ],
# [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 1 ],
# color='b', marker='+', label=pos_response )
#Zplt.set_xlim( [ np.amin(Z[:,1]), np.amax(Z[:,1]) ] )
#Zplt.set_ylim( [ np.amin(Z[:,2]), np.amax(Z[:,2]) ] )
#Zplt.set_title(r"standardized $\mathbf{Z},\beta$ fitting space")
#
#Xplt.legend(loc=2)
#Zplt.legend(loc=2)
#Xplt.legend(loc='center left', bbox_to_anchor=(1.10, 0.50),
#          ncol=1, fancybox=True, shadow=True)
#plt.show()
#plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, facecolor=fig.get_facecolor(), edgecolor="None", metadata=None)

