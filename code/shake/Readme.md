Raspberry Shake Detection Statistics
====================================

A complete analysis of these data using matlab and fortran is already available [here](https://gitlab.com/imhoffman/shake).
In this repository, I am reanalyzing the data as a test of my new [tools](https://gitlab.com/imhoffman/data-science/tree/master/code/tools).

### detection classification

In distance&ndash;magnitude space, here is the fit that is quadratic in both features.
The fit is executed and plotted in semi-log space ([here is the code](shake.py), using my homebrewed [bclearn](https://gitlab.com/imhoffman/data-science/tree/master/code/tools/bclearn) package).

![shake-1.png](../../images/shake-1.png)

![shake-2.png](../../images/shake-2.png)
