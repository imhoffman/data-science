import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
from bclearn import logistic
from bclearn.general import *

stuff = pd.read_csv("../../data/misc/Absenteeism_at_work.csv", delimiter=';')

###
###  data selection and conditioning
###
#nf = stuff.dropna()
nf = stuff
#nf = nf.astype(int, errors="ignore")
#nf = nf.apply(pd.to_numeric, errors="ignore")
#nf = nf.drop(
#    nf.iloc[ [ i for i in range(len(nf))
#        if ( nf.iloc[i,0] != "M" and nf.iloc[i,0] != "F"
#              or nf.iloc[i,3] > 0.40 ) ], : ].index )
#             nf.iloc[i,0] > 18 or
#             nf.iloc[i,3] > 150
#             or nf.iloc[i,6] == "?" ) ], : ].index
#     ) 
#nf = nf.apply(pd.to_numeric, errors="ignore")
nf.reset_index(drop=True, inplace=True)
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


##  load up fitting vectors
##   the features/predictors
#3. Month of absence 
#4. Day of the week (Monday (2), Tuesday (3), Wednesday (4), Thursday (5), Friday (6)) 
#5. Seasons (summer (1), autumn (2), winter (3), spring (4)) 
#6. Transportation expense 
#7. Distance from Residence to Work (kilometers) 
#8. Service time 
#9. Age 
#10. Work load Average/day 
#11. Hit target 
#12. Disciplinary failure (yes=1; no=0) 
#13. Education (high school (1), graduate (2), postgraduate (3), master and doctor (4)) 
#14. Son (number of children) 
#15. Social drinker (yes=1; no=0) 
#16. Social smoker (yes=1; no=0) 
#17. Pet (number of pet) 
#18. Weight 
#19. Height 
#20. Body mass index 
#21. Absenteeism time in hours (target) 
fea1 = 12; x_label = "Education"
fea2 = 19; y_label = "Body mass index"
X = np.array( [ np.ones( len(nf) ),
               nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea2].to_numpy(),
               nf.iloc[:,fea1].to_numpy()*nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea2].to_numpy()*nf.iloc[:,fea2].to_numpy(),
               ] ).T
boundary = logistic.quadratic_boundary
#boundary = logistic.linear_boundary

##  the response
y = np.array( nf.iloc[:,15].to_numpy(), ndmin=2 ).T
pos_response = "smoker"
neg_response = "non-smoker"

m,n = X.shape

##  standardization
Z, X_mu, X_std = standardizer( X )

##  initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T
#beta = np.array( [ 1, 1, 1, 0, 0 ], ndmin=2 ).T

##  learning loop
beta = gradient_descent(
         y, Z, beta, logistic.hypothesis, logistic.cost,
         alpha = 2.00E-7, tol = 3.0E-08, max_iter = 625000, report = 20000 )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std )

##
##  plotting
##
plotter ( X, theta, y, boundary,
        Plogit = [ 0.55, 0.75, 0.95 ],
        styles = [ "dotted", "dashed", "solid" ],
        colors = [ 'r', 'g', 'b' ],
        prediction = logistic.prediction,
        x_label = x_label, y_label = y_label,
        adj = [ -1.0, +1.0, 0.0, 0.0 ],
        pos_response = pos_response, neg_response = neg_response,
        show = False, sized = True, size_d2 = True,
        hist = True,
        bins = [ np.arange(1,6)-0.5, 15 ],
        hcolors = [ "Reds", "Greens" ], matched_bins = True )

