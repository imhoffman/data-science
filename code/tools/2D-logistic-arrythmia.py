import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
from bclearn import logistic
from bclearn.general import *

stuff = pd.read_csv("../../data/misc/arrhythmia.data")

###
###  data selection and conditioning
###
nf = stuff.dropna()
##nf = nf.astype(int, errors="ignore")
#nf = nf.apply(pd.to_numeric, errors="ignore")
##nf = nf.apply(pd.to_numeric, errors="coerce")
# for arrythmia set, drop super-tall heights, huge masses, and juvenile ages
nf = nf.drop(
    nf.iloc[ [ i for i in range(len(nf))
        if ( nf.iloc[i,2] > 250 or
             nf.iloc[i,0] < 18 or
             nf.iloc[i,3] > 150
             or nf.iloc[i,14] == "?" ) ], : ].index
     ) 
nf = nf.apply(pd.to_numeric, errors="ignore")
nf = nf.dropna()
nf.reset_index(drop=True, inplace=True)
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


###
###  logistic fitting
###
##  load up fitting vectors
##   the features/predictors
#      1 Age: Age in years , linear
#      2 Sex: Sex (0 = male; 1 = female) , nominal
#      3 Height: Height in centimeters , linear
#      4 Weight: Weight in kilograms , linear
#      5 QRS duration: Average of QRS duration in msec., linear
#      6 P-R interval: Average duration between onset of P and Q waves in msec., linear
#      7 Q-T interval: Average duration between onset of Q and offset of T waves in msec., linear
#      8 T interval: Average duration of T wave in msec., linear
#      9 P interval: Average duration of P wave in msec., linear
#     Vector angles in degrees on front plane of:, linear
#     10 QRS
#     11 T
#     12 P
#     13 QRST
#     14 J
#     15 Heart rate: Number of heart beats per minute ,linear
fea1 = 2; x_label = "Height (cm)";
fea2 = 3; y_label = "Weight (cm)";
X = np.array( [ np.ones( len(nf) ),
               nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea2].to_numpy(),
#               nf.iloc[:,fea1].to_numpy()*nf.iloc[:,fea1].to_numpy(),
#               nf.iloc[:,fea2].to_numpy()*nf.iloc[:,fea2].to_numpy(),
               ] ).T
#boundary = logistic.quadratic_boundary
boundary = logistic.linear_boundary

##  the response
y = np.array( [ nf.iloc[:,1].to_numpy() ] ).T   # sex
pos_response = "female"
neg_response = "male"

m,n = X.shape

##  standardization
Z, X_mu, X_std = standardizer( X )

##  initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T

##  learning loop
beta = gradient_descent(
         y, Z, beta, logistic.hypothesis, logistic.cost,
         alpha = 2.50E-5, tol = 1.0E-10, max_iter =1075000, report = 25000 )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std )

##
##  plotting
##
x1 = np.linspace( np.amin(X[:,1],axis=0), np.amax(X[:,1],axis=0), num=8192 )
z1 = np.linspace( np.amin(Z[:,1],axis=0), np.amax(Z[:,1],axis=0), num=8192 )
fig, (Xplt,Zplt) = plt.subplots(1, 2, sharey=False, tight_layout=True)

#  first subplot
Plogit = [ 0.45, 0.50, 0.55 ]
Xplt.plot( x1, boundary( x1, theta, Plogit[0] ), 'g:',
   label="$p$ = %.2f"%Plogit[0] )
Xplt.plot( x1, boundary( x1, theta, Plogit[1] ), 'g--',
   label="$p$ = %.2f"%Plogit[1] )
Xplt.plot( x1, boundary( x1, theta, Plogit[2] ), 'g-',
   label="$p$ = %.2f"%Plogit[2] )
Xplt.scatter(
 [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
 [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
 color='b', marker='1', label=neg_response )
Xplt.scatter(
 [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
 [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
 color='m', marker='2', label=pos_response )
Xplt.set_xlim( [ np.amin(X[:,1]), np.amax(X[:,1]) ] )
Xplt.set_ylim( [ np.amin(X[:,2]), np.amax(X[:,2]) ] )
Xplt.set_title(r"unscaled $\mathbf{X},\theta$ space")
Xplt.set_xlabel(x_label)
Xplt.set_ylabel(y_label)

#  second subplot
Zplt.scatter(
 [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 0 ],
 [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 0 ],
 color='b', marker='2', label=neg_response )
Zplt.scatter(
 [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 1 ],
 [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 1 ],
 color='m', marker='1', label=pos_response )
Zplt.set_xlim( [ np.amin(Z[:,1]), np.amax(Z[:,1]) ] )
Zplt.set_ylim( [ np.amin(Z[:,2]), np.amax(Z[:,2]) ] )
Zplt.set_title(r"standardized $\mathbf{Z},\beta$ fitting space")

Xplt.legend(loc=4)
Zplt.legend(loc=4)
#Xplt.legend(loc='center left', bbox_to_anchor=(1.10, 0.50),
#          ncol=1, fancybox=True, shadow=True)
#plt.show()
plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, facecolor=fig.get_facecolor(), edgecolor="None", metadata=None)

