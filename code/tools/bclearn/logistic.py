import numpy as np

###
###  logistic fitting
###
def sigmoid ( z ):
  return np.divide( 1.0, np.add( 1.0, np.exp( -z ) ) )

def hypothesis ( A, B ):
  return sigmoid( np.dot( A, B ) )

def prediction ( X, theta, P ):
  vals = np.dot( X, theta )
  return vals > np.log( P/( 1.0-P ) )
#  combine with sum( predictions( X, theta, P ) == y )[0] / len( y )

###
###  model boundaries
###
def quadratic_boundary ( x, a, P=0.5, pos=True ):
  '''
  Boundary for plotting after having fitted quadratic dependence in both of two features. The array of feature columns must have been
  X[:,0] = 1
  X[:,1] = x1     <-- the horizontal axis
  X[:,2] = x2     <-- the vertical axis
  X[:,3] = x1*x1
  X[:,4] = x2*x2

  Inputs
  ------
  x  : vector of independent variable values.
  a  : vector of length 1+2*Nfeatures of the fitted coefficients
  P  : logit probability of boundary; scalar
  pos: boolean for whether the positive or negative root is desired

  Returns
  -------
  vector of length len(x) containing the function values of the boundary
  '''
  C = a[0] + a[1]*x + a[3]*x**2 - np.log(P/(1.0-P))
  if ( pos ) :
    return ( -1.0*a[2] + np.sqrt( a[2]*a[2] - 4.0*a[4]*C ) )/2.0/a[4]
  else:
    return ( -1.0*a[2] - np.sqrt( a[2]*a[2] - 4.0*a[4]*C ) )/2.0/a[4]

def linear_boundary ( x, a, P=0.5 ):
  '''
  Boundary for plotting after having fitted linear dependence in both of two features.

  Inputs
  ------
  x  : vector of independent variable values.
  a  : vector of length 1+Nfeatures of the fitted coefficients
  P  : logit probability of boundary; scalar

  Returns
  -------
  _  : vector of length len(x) containing the function values of the boundary
  '''
  return ( np.log(P/(1.0-P)) - x*a[1] - a[0] )/a[2]

###
###  cost function
###
def cost ( y, Z, beta ):
  '''
  Cost function for a sigmoid classification of 1,0 values. To be called from the fitting routine.

  Inputs
  ------
  y       : vector of classified responses
  Z       : array of feature columns, including a zeroth "intercept" column of all ones
  beta    : vector of length Z.shape[1] of fitting coefficients

  Returns
  -------
  S       : the cost, scalar float
  '''
  h = hypothesis
  m = Z.shape[0]
  S = -1.0/m * np.sum(
                     y * np.log( h(Z,beta) )
             + (1.0-y) * np.log( 1.0-h(Z,beta) ) )
  return S

