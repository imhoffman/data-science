import numpy as np
import matplotlib.pyplot as plt

###
###  feature standardization
###
#  a very nice explanation at
#  https://stats.stackexchange.com/questions/74622/converting-standardized-betas-back-to-original-variables

def standardizer( X, y=[] ):
    '''
    Scale features to near unity and center near zero prior to fitting. This procedure is paired with unstandardizer.

    Inputs
    ------
    X: The array of feature columns. The zeroth column is expected to be the "intercept" column of all ones.
    y: Optional. The vector of responses.

    Returns
    -------
    _    : An array with the dimensions of X with each column individually standardized.
    X_mu : A vector of length X.shape[1] containing the numpy.mean of each column of X.
    X_std: A vector of length X.shape[1] containing the numpy.std of each column of X. The zeroth entry is set to one (since the std of the expected column of ones is zero).
    _    : Optional. A standardized vector or y.
    y_mu : Optional. The numpy.mean of the response vector y. Scalar float.
    y_std: Optional. The numpy.std of the response vector y. Scalar float.
    '''
    X_mu  = np.array( [ np.mean( X, axis=0 ) ], ndmin=2 ).T
    X_std = np.array( [  np.std( X, axis=0 ) ], ndmin=2 ).T
    X_std[0] = 1.0                    # reset the ones column
    X_mu[0]  = 0.0                    # reset the ones column
    if len(y) :
      y_mu  = np.mean( y )
      y_std =  np.std( y )
      return ( X - X_mu.T ) / X_std.T, X_mu, X_std, ( y - y_mu ) / y_std, y_mu, y_std
    else:
      return ( X - X_mu.T ) / X_std.T, X_mu, X_std


def unstandardizer( Zcoeffs, X_mu, X_std, y_mu=None, y_std=None ):
    '''
    Undo the feature scaling following fitting. This procedure is paired with standardizer.

    Inputs
    ------
    Zcoeffs: The vector of fitting coefficients in scaled space.
    X_mu   : The vector of np.mean of the feature columns prior to scaling.
    X_std  : The vector of np.std of the feature columns prior to scaling.
    y_mu   : Optional. The np.mean of the response vector prior to scaling. Scalar.
    y_std  : Optional. The np.std of the response vector prior to scaling. Scalar.

    Returns
    -------
    Xcoeffs: The vector of fitting coefficients in the original, unscaled space.
    '''
    #X = Z*X_std + X_mu
    Xcoeffs = np.array( [ np.ones( len(Zcoeffs) ) ], ndmin=2 ).T
    if y_mu != None and y_std != None :
      Xcoeffs[0] = Zcoeffs[0]*y_std + y_mu - y_std*np.sum( Zcoeffs[1:] * X_mu[1:] / X_std[1:] )
      Xcoeffs[1:] = Zcoeffs[1:] / X_std[1:] * y_std
    else:
      Xcoeffs[0] = Zcoeffs[0] - np.sum( Zcoeffs[1:] * X_mu[1:] / X_std[1:] )
      Xcoeffs[1:] = Zcoeffs[1:] / X_std[1:]
    return Xcoeffs

###
###  minimization
###
def gradient_descent(
         y, Z, beta, hypothesis, cost,
         alpha=3.0E-4, tol=1.0E-07,
         max_iter=150000, report=3000 ):
  '''
  Iterative loop for minimization via gradient descent.

  Inputs
  ------
  y         : vector of responses
  Z         : array of feature columns, including a zeroth "intercept" column of all ones
  beta      : vector of length Z.shape[1] of fitting coefficients
  hypothesis: name of hypothesis function that accepts arguments ( Z, beta )
  cost      : name of cost function that accepts arguments ( y, Z, beta )
  alpha     : learning rate
  tol       : stopping tolerance (relative gain from preceding iteration)
  max_iter  : maximum number of iterations, scalar int
  report    : progress/cost report printed when _%report == 0

  Returns
  -------
  beta    : vector of coefficients when loop terminates
  '''
  h = hypothesis
  m = Z.shape[0]
  gain = tol + 1.
  count = 0
  S = 1.
  while gain > tol and count < max_iter:
    beta = beta - alpha/m * np.dot( ( h(Z,beta) - y ).T, Z ).T
    count += 1
    S = cost( y, Z, beta )
    gain = abs( ( gain - S )/gain )
    if count % report == 0:
      print("After %7d counts the sum of the cost is %.4f" % (count, S))

  print("\n %s after %d iterations\n\n" % ("Gave up" if count==max_iter else "Converged",count) )
  return beta


def normal_equations( y, Z ):
  '''
  Linear-algebra symbolic closed form for least-squares extremum. Uses the pseudo-inverse np.pinv.

  Inputs
  ------
  y : vector of classified responses
  Z : array of feature columns, including a zeroth "intercept" column of all ones

  Returns
  -------
  _ : vector of length Z.shape[1] of coefficients
  '''
  #vec = np.linalg.pinv( Z.T.dot( Z ) ).dot( Z.T ).dot( y )
  #return np.asarray( vec ).reshape(-1)
  return np.linalg.pinv( Z.T.dot( Z ) ).dot( Z.T ).dot( y )

###
### diagnostics
###
def R_squared ( X, theta, y ):
  SStot = np.sum( ( y - np.mean( y ) )**2 )
  SSreg = np.sum( ( X@theta - np.mean( y ) )**2 )
  SSres = np.sum( ( y - X@theta )**2 )
  return 1 - SSres/SStot

def chi_squared ( X, theta, y ):
  return np.sum( ( ( y - X@theta )/np.std( y ) )**2 )

def reduced_chi_squared ( X, theta, y ):
  return chi_squared( X, theta, y )/( len(y) - X.shape[1] )

###
### plotting
###
def sizer ( i, X, y, d2=False ) :
  '''
  Count instances of an event in the set and return value for sizing the plotted point. This is currently very inefficient in that it re-runs the sum for every index rather than buildling a look-up table.

  Inputs
  ------

  Returns
  -------
  '''
  sizep = 0
  xp = X[i,1]
  yp = X[i,2]
  response = y[i]
  for i in range(X.shape[0]):
    if response==0:
      if d2:
        if xp==X[i,1] and yp==X[i,2]:
          sizep = sizep + 1
      else:
        if xp==X[i,1]:
          sizep = sizep + 1
    elif response==1:
      if d2:
        if xp==X[i,1] and yp==X[i,2]:
          sizep = sizep + 1
      else:
        if xp==X[i,1]:
          sizep = sizep + 1
    else:
      print("\n unknown classification\n")
  return sizep

def plotter ( X, theta, y, boundary, Plogit, styles, colors, prediction,
             x_label=None, y_label=None,
             adj = [ 0, 0, 0, 0 ], 
             pos_response=None, neg_response=None,
             markers = [ '2', '1' ],
             show=True, sized=False, size_d2=False, aspect="auto",
             hist=False, bins=None, hcolors=None, matched_bins=False,
             Z=[], beta=[], yZ=[], Zadj=[0,0,0,0] ):
  '''
  Modularized, semi-flexible external plotting function. This really can't be all things to all people. And, the second-pane Z plot is a work in progress.

  Inputs
  ------
  X        : The array of feature columns.
  theta    : The vector of feature coefficients of length X.shape[1]
  y        : The vector of response values of length X.shape[0]
  boundary : The function pointer to the analytical hyperplane.
  Plogit   : vector of probability boundaries to plot
  styles   : vector of line styles to use for probability boundaries
  colors   : colors to use for affirmative, negative, and boundary
  x_label  : Optional. string for horizontal axis
  y_label  : Optional. string for vertical axis
  adj      : Optional. vector of four value with which to amend xlim and ylim
  pos_resp : Optional. string for affirmative classification
  neg_resp : Optional. string for negative axis
  show     : Optional. boolean for windowed (True) or png (False) output
  sized    : Optional. boolean for whether to scale point sizes
  size_2d  : Optional. boolean for whether to scale point sizes in x or x,y
  ...
  '''
  x1 = np.linspace( np.amin(X[:,1],axis=0)+adj[0],
                    np.amax(X[:,1],axis=0)+adj[1],
                    num=8192 )

  if len(Z):
    z1 = np.linspace( np.amin(Z[:,1],axis=0)+Zadj[0],
                      np.amax(Z[:,1],axis=0)+Zadj[1],
                      num=8192 )
    fig, (Xplt,Zplt) = plt.subplots(1, 2, sharey=False, tight_layout=True)
  else:
    fig, Xplt = plt.subplots(1, 1, sharey=False, tight_layout=True)

  if hist:
    if matched_bins:
      h, xedges, yedges, image = Xplt.hist2d(
       [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
       [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
       cmin=1, cmap=hcolors[0], alpha=0.5, bins=bins  )
      Xplt.hist2d(
       [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
       [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
       bins = [xedges,yedges],
       cmin=1, cmap=hcolors[1], alpha=0.5 )
    else:
      Xplt.hist2d(
       [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
       [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
       cmin=1, cmap=hcolors[0], alpha=0.5, bins=bins  )
      Xplt.hist2d(
       [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
       [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
       cmin=1, cmap=hcolors[1], alpha=0.5, bins=bins  )

  Xplt.scatter(
   [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
   [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
   color=colors[0], marker=markers[0], label=pos_response,  # affirm. is up-pointing
   s = [ sizer(i,X,y,size_d2) for i in range(X.shape[0]) ] if sized else None )
  Xplt.scatter(
   [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
   [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
   color=colors[1], marker=markers[1], label=neg_response,
   s = [ sizer(i,X,y,size_d2) for i in range(X.shape[0]) ] if sized else None )
  if len(Z):
    Zplt.scatter(
     [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 1 ],
     [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 1 ],
     color=colors[0], marker=markers[0], label=pos_response )
    Zplt.scatter(
     [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 0 ],
     [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 0 ],
     color=colors[1], marker=markers[1], label=neg_response )

  for (p,ls) in zip(Plogit,styles):
    Xplt.plot( x1, boundary( x1, theta, p ),
               color=colors[2], linestyle=ls,
               label="$p$=%.2f (%2.1f%%)" % ( p,
                 sum( prediction(X,theta,p) == y )[0] / len(y) * 100.0 ) )

  Xplt.set_xlim( [ np.amin(X[:,1])+adj[0], np.amax(X[:,1])+adj[1] ] )
  Xplt.set_ylim( [ np.amin(X[:,2])+adj[2], np.amax(X[:,2])+adj[3] ] )
  Xplt.set_title(r"unscaled $\mathbf{X},\theta$ space")
  Xplt.set_xlabel(x_label)
  Xplt.set_ylabel(y_label)

  Xplt.set(aspect=aspect)
  #Xplt.legend(loc=4)
  Xplt.legend(loc='center left', bbox_to_anchor=(1.05, 0.50),
          ncol=1, fancybox=True, shadow=True)

  if show:
    plt.show()
  else:
    plt.savefig("out.png", dpi="figure", papertype="letter",
                format="png", transparent=False,
                bbox_inches=None, pad_inches=0.1,
                facecolor=fig.get_facecolor(),
                edgecolor="None", metadata=None)

