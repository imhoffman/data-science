import sys

if sys.version_info[0] < 3 or sys.version_info[1] < 6 :
  print("\n\n at least version 3.6 is required for bclearn\n\n")
  sys.exit(1)

import numpy as np

print("\n loading bclearn\n import numpy as np\n import matplotlib.pyplot as plt\n\n")
