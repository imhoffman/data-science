import numpy as np

###
###  linear least-squares
###
def hypothesis ( A, B ):
  '''
  Linear dependence of the columns of A combined with the coefficients B

  Inputs
  ------
  A: array of column of independent features, including the zeroth column of ones for the "intercept"
  B: vector of length A.shape[1] of feature coefficients, including the B[0] intercept

  Returns
  -------
  _: the linear combination of features
  '''
  return np.dot( A, B )

###
###  model boundaries
###
def quadratic_boundary ( x, a ):
  '''
  Best-fit function for plotting after having fitted quadratic dependence of a single feature.

  Inputs
  ------
  x  : vector of independent variable values.
  a  : vector of length 1+2*Nfeatures of the fitted coefficients

  Returns
  -------
  _  : vector of length len(x) containing the function values of the boundary
  '''
  return a[2]*(x**2) + a[1]*x + a[0]            # a[2]*x*x  !=  a[2]*x**2


def linear_boundary ( x, a ):
  '''
  Best-fit function for plotting after having fitted linear dependence in one feature.

  Inputs
  ------
  x  : vector of independent variable values.
  a  : vector of length 1+Nfeatures of the fitted coefficients

  Returns
  -------
  _  : vector of length len(x) containing the function values of the boundary
  '''
  return a[1]*x + a[0]

###
###  cost/objective function
###
def cost ( y, Z, beta ):
  '''
  Cost function for the squared difference between the hypothesis and a linear combination of features. To be called from an iterative least-squares minimizing/fitting routine.

  Inputs
  ------
  y       : vector of responses
  Z       : array of feature columns, including a zeroth "intercept" column of all ones
  beta    : vector of length Z.shape[1] of fitting coefficients

  Returns
  -------
  S       : the cost, scalar float
  '''
  h = hypothesis
  m = Z.shape[0]
  # dot it with itself rather than sum
  S = 1.0/m * np.dot( ( h(Z,beta)-y ).T, h(Z,beta)-y )
  return S

