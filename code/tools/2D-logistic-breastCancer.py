import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
from bclearn import logistic
from bclearn.general import *

stuff = pd.read_csv("../../data/misc/breast-cancer-wisconsin.data")

###
###  data selection and conditioning
###
nf = stuff.dropna()
#nf = nf.astype(int, errors="ignore")
#nf = nf.apply(pd.to_numeric, errors="ignore")
nf = nf.apply(pd.to_numeric, errors="coerce")
# for arrythmia set, drop super-tall heights, huge masses, and juvenile ages
#nf = nf.drop(
#    nf.iloc[ [ i for i in range(len(nf))
#        if ( nf.iloc[i,2] > 250 or
#             nf.iloc[i,0] > 18 or
#             nf.iloc[i,3] > 150
#             or nf.iloc[i,6] == "?" ) ], : ].index
#     ) 
#nf = nf.apply(pd.to_numeric, errors="ignore")
nf = nf.dropna()
nf.reset_index(drop=True, inplace=True)
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


##  load up fitting vectors
##   the features/predictors
#   #  Attribute                     Domain
#   -- -----------------------------------------
#   1. Sample code number            id number
#   2. Clump Thickness               1 - 10
#   3. Uniformity of Cell Size       1 - 10
#   4. Uniformity of Cell Shape      1 - 10
#   5. Marginal Adhesion             1 - 10
#   6. Single Epithelial Cell Size   1 - 10
#   7. Bare Nuclei                   1 - 10
#   8. Bland Chromatin               1 - 10
#   9. Normal Nucleoli               1 - 10
#  10. Mitoses                       1 - 10
#  11. Class:                        (2 for benign, 4 for malignant)

fea1 = 5; x_label = "Clump Thickness"
fea2 = 8; y_label = "Normal Nucleoli"
X = np.array( [ np.ones( len(nf) ),
               nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea2].to_numpy(),
               nf.iloc[:,fea1].to_numpy()*nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea2].to_numpy()*nf.iloc[:,fea2].to_numpy(),
               ] ).T
boundary = logistic.quadratic_boundary
#boundary = logistic.linear_boundary


##  the response: map 2,4 to 0,1 and throw a -1 if there's a problem
y = np.array(
     nf.iloc[:,10].apply(
      lambda x: 1.0 if x==4 else (0.0 if x==2 else -1) ).to_numpy(), ndmin=2 ).T
pos_response = "malignant"
neg_response = "benign"

m,n = X.shape

##  standardization
Z, X_mu, X_std = standardizer( X )

##  initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T

##  learning loop
beta = gradient_descent(
         y, Z, beta, logistic.hypothesis, logistic.cost,
         alpha = 2.10E-6, tol = 2.0E-07, max_iter = 525000, report = 20000 )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std )

##
##  plotting
##
plotter ( X, theta, y, boundary,
        Plogit = [ 0.55, 0.75, 0.95 ],
        styles = [ "dotted", "dashed", "solid" ],
        colors = [ 'r', 'g', 'b' ],
        prediction = logistic.prediction,
        x_label = x_label, y_label = y_label,
        adj = [ -1.0, +1.0, -1.0, +1.0 ],
        pos_response = pos_response, neg_response = neg_response,
        show = False, sized = True, size_d2 = True,
        aspect = 1,
        hist = True,
        bins = [ np.arange(1,12)-0.5, np.arange(1,12)-0.5 ],
        hcolors = [ "Reds", "Greens" ] )

