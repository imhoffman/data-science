import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 

stuff = pd.read_csv("../../data/misc/abalone.data")

###
###  data selection and conditioning
###
#nf = stuff.dropna()
nf = stuff
#nf = nf.astype(int, errors="ignore")
#nf = nf.apply(pd.to_numeric, errors="ignore")
# for arrythmia set, drop super-tall heights, huge masses, and juvenile ages
nf = nf.drop(
    nf.iloc[ [ i for i in range(len(nf))
        if ( nf.iloc[i,0] != "M" and nf.iloc[i,0] != "F"
              or nf.iloc[i,3] > 0.40 ) ], : ].index )
#             nf.iloc[i,0] > 18 or
#             nf.iloc[i,3] > 150
#             or nf.iloc[i,6] == "?" ) ], : ].index
#     ) 
#nf = nf.apply(pd.to_numeric, errors="ignore")
nf.reset_index(drop=True, inplace=True)
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


###
###  logistic fitting
###
def sigmoid ( z ):
  return np.divide( 1.0, np.add( 1.0, np.exp( -z ) ) )

def h ( A, B ):
  return sigmoid( np.dot( A, B ) )

##  load up fitting vectors
##   the features/predictors
# 0      Sex             nominal                 M, F, and I (infant)
# 1      Length          continuous      mm      Longest shell measurement
# 2      Diameter        continuous      mm      perpendicular to length
# 3      Height          continuous      mm      with meat in shell
# 4      Whole weight    continuous      grams   whole abalone
# 5      Shucked weight  continuous      grams   weight of meat
# 6      Viscera weight  continuous      grams   gut weight (after bleeding)
# 7      Shell weight    continuous      grams   after being dried
# 8      Rings           integer                 +1.5 gives the age in years
X = np.array( [ np.ones( len(nf) ),
               nf.iloc[:,6].to_numpy(),
               nf.iloc[:,3].to_numpy(),
               nf.iloc[:,6].to_numpy()*nf.iloc[:,6].to_numpy(),
               nf.iloc[:,3].to_numpy()*nf.iloc[:,3].to_numpy(),
               ] ).T
x_label = "Viscera weight (g)"
y_label = "Height (mm)"

##  the response: map 2,4 to 0,1 and throw a -1 if there's a problem
y = np.array( nf.iloc[:,0].apply( lambda x: 1.0 if x=="F" else (0.0 if x=="M" else -1) ).to_numpy(), ndmin=2 ).T

m,n = X.shape

##  standardization
def scaler(orig):
    mu  = np.mean( orig, axis=0 )
    std =  np.std( orig, axis=0 )
    std[0] = 1   # for the ones column
    return (orig - mu)/std, mu, std

Z, X_mu, X_std = scaler(X)

##  beta vector of features for Z space
beta = np.array( [ np.ones(n) ]).T

##  tuning constants
tol = 1.0E-7
alpha = 3.00E-5
max_iter = 80000

##  learning loop
gain = tol + 1.
count = 0
S = 1.
while gain > tol and count < max_iter:
  beta = beta - alpha/m * np.dot( ( h(Z,beta) - y ).T, Z ).T
  count = count + 1
  S = -1.0/m * np.sum(
                     y * np.log( h(Z,beta) )
             + (1.0-y) * np.log( 1.0-h(Z,beta) ) )
  gain = abs( ( gain - S )/gain )
  if count % 3000 == 0:
    print("After %6d counts the sum of the cost is %.4f" % (count, S))

print("\n %s after %d iterations\n\n" % ("Gave up" if count==max_iter else "Converged",count) )
print(beta)

##  unscale: map to X--theta space from Z--beta space
X = Z*X_std + X_mu

def unscaler(coeffs):
    out = np.array( np.ones( len(coeffs) ) )
    out[0] = coeffs[0] - np.sum( coeffs[1:,0]*X_mu[1:]/X_std[1:] )
    out[1:] = coeffs[1:,0]/X_std[1:]
    return out
    # the ,0 is needed in coeffs[1:,0] in order to match 
    # the dimension of the X_mu and X_std column for elem-by-elem div
    # or else include an ndmin=2 somewhere upstream

theta = unscaler(beta)

##
##  plotting
##
def sizer ( i, X, y ) :
  sizep = 0
  xp = X[i,1]
  yp = X[i,2]
  response = y[i]
  for i in range(X.shape[0]):
    if response==0:
      if xp==X[i,1] and yp==X[i,2]:
        sizep = sizep + 1
    elif response==1:
      if xp==X[i,1] and yp==X[i,2]:
        sizep = sizep + 1
    else:
      print("\n unknown classification\n")
  return sizep

def quad_bound ( x, a, P ):
    # a is the vector of coefficients as per how X was loaded up
    C = a[0] + a[1]*x + a[3]*x*x - np.log(P/(1.0-P))
    return ( -1.0*a[2] + np.sqrt( a[2]*a[2] - 4.0*a[4]*C ) )/2.0/a[4]
    #return ( -1.0*a[2] - np.sqrt( a[2]*a[2] - 4.0*a[4]*C ) )/2.0/a[4]

x1 = np.linspace( np.amin(X[:,1],axis=0), np.amax(X[:,1],axis=0), num=8192 )
#z1 = np.linspace( np.amin(Z[:,1],axis=0), np.amax(Z[:,1],axis=0), num=8192 )
#fig, (Xplt,Zplt) = plt.subplots(1, 2, sharey=False, tight_layout=True)
fig, Xplt = plt.subplots(1, 1, sharey=False, tight_layout=True)

#  first subplot
Xplt.plot( x1, quad_bound( x1, theta, 0.25 ), 'g:')
Xplt.plot( x1, quad_bound( x1, theta, 0.50 ), 'g--')
Xplt.plot( x1, quad_bound( x1, theta, 0.75 ), 'g-')
#Xplt.hist2d(
# [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
# [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
# cmin=1, cmap='Blues', alpha=0.5,
# bins=[ np.arange(1,12)-0.5, np.arange(1,12)-0.5 ]  )
#Xplt.hist2d(
# [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
# [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
# cmin=1, cmap='Reds', alpha=0.5,
# bins=[ np.arange(1,12)-0.5, np.arange(1,12)-0.5 ]  )
Xplt.scatter(
 [ X[i,1] for i in range(X.shape[0]) if y[i] == 0 ],
 [ X[i,2] for i in range(X.shape[0]) if y[i] == 0 ],
 color='b', marker='1', label="male" )
# s = [ sizer(i,X,y) for i in range(X.shape[0]) ] )
Xplt.scatter(
 [ X[i,1] for i in range(X.shape[0]) if y[i] == 1 ],
 [ X[i,2] for i in range(X.shape[0]) if y[i] == 1 ],
 color='r', marker='2', label="female" )
# s = [ sizer(i,X,y) for i in range(X.shape[0]) ] )
Xplt.set_xlim( [ np.amin(X[:,1]), np.amax(X[:,1]) ] )
Xplt.set_ylim( [ np.amin(X[:,2]), np.amax(X[:,2]) ] )
Xplt.set_title(r"unscaled $\mathbf{X},\theta$ space")
Xplt.set_xlabel(x_label)
Xplt.set_ylabel(y_label)

#  second subplot
#Zplt.plot( z1, quad( z1, beta, 0.75 ), 'b')
#Zplt.scatter(
# [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 0 ],
# [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 0 ],
# color='g', marker='2', label="benign")
#Zplt.scatter(
# [ Z[i,1] for i in range(Z.shape[0]) if y[i] == 1 ],
# [ Z[i,2] for i in range(Z.shape[0]) if y[i] == 1 ],
# color='r', marker='1', label="malignant")
#Zplt.set_xlim( [ np.amin(Z[:,1])-1, np.amax(Z[:,1])+1 ] )
#Zplt.set_ylim( [ np.amin(Z[:,2])-1, np.amax(Z[:,2])+1 ] )
#Zplt.set_title(r"standardized $\mathbf{Z},\beta$ fitting space")
#
#Xplt.set(aspect=1)
#Zplt.set(aspect=1)
Xplt.legend(loc=4)
#Zplt.legend(loc=4)
#plt.show()
plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, frameon=False, metadata=None)

