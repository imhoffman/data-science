Basic Tools Coded By Hand
=========================

Learning by doing.
In order to better learn python and go (and to better learn the topics in statistical analysis that haven't already come up in my research), I am attempting to write a general package/library.
I call the package [bclearn](bclearn).
I hope that it will include sensible implementations of all of the basic scaling and fitting routines, as well as maybe some useful plotting.
However, it is not my intention to re-implement numpy or matplotlib&mdash;and I plan to use pandas only sparingly for initial munging and for reading proprietary data formats (although [I've reverse-engineered binary formats before](https://gitlab.com/imhoffman/SL2PK)).

The codes here import my bclearn.
My testing regimen is to run my stuff on whatever generic data sets that I can find at the UCI repository.
The quadratic logistic boundary is something that I had already written in octave/matlab previously for [my Raspberry Shake analysis](https://gitlab.com/imhoffman/shake).

## logistic regression

### Breast Cancer

From [the breast cancer data set](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Original%29):

Classification of malignant and benign tumors ([code](2D-logistic-breastCancer.py))

![breast-cancer-colormap.png](../../images/breast-cancer-colormap.png)

![breast-cancer-colormap-2.png](../../images/breast-cancer-colormap-2.png)

### Absenteeism

From [the absenteeism data set](https://archive.ics.uci.edu/ml/datasets/Absenteeism+at+work):

Drinkers and smokers as a function of body mass index ([code](2D-logistic-absenteesism.py)),

![absenteeism-2.png](../../images/absenteeism-2.png)

![absenteeism-1.png](../../images/absenteeism-1.png)

### Arrhythmia

From [the arrhythmia data set](https://archive.ics.uci.edu/ml/datasets/Arrhythmia):

A one-feature classification ([code](2D-logistic-arrhythmia.py))

![arrhythmia-sex-height.png](../../images/arrhythmia-sex-height.png)

and a two-feature (straight-line) classification ([code](1D-logistic-arrhythmia.py)).

![arrhythmia-sex-height-weight.png](../../images/arrhythmia-sex-height-weight.png)


## Linear Least Squares

### parabola

I wanted a parabola and they don't have any physics data on the UCI server.
And, I couldn't find any physics data easily on the web.
But, in searching, I found [this article](https://www.wired.com/2013/06/water-hose-projectile-motion/)&mdash;which provides everything *but* the data&mdash;and then I picked my own points with [this Java application](https://physlets.org/tracker/) on the image from the Wired page in order to generate these uncalibrated position data.
([code](LLS-water.py))

![water-1.png](../../images/water-1.png)

I also found [these sample data](http://web.cecs.pdx.edu/~gerry/MATLAB/plotting/loadingPlotData.html#loadAndPlot) that are likely *not* a parabola.
The gradient descent fit agrees very well with the computation of the pseudo-inverse of the closed-form symbolic solution.

![water-2.png](../../images/water-2.png)

