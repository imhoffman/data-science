import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
from bclearn import logistic
from bclearn.general import *

stuff = pd.read_csv("../../data/misc/arrhythmia.data")

###
###  data selection and conditioning
###
nf = stuff.dropna()
# for arrythmia set, drop tall heights and young ages
nf = nf.drop(
    nf.iloc[ [ i for i in range(len(nf)) if ( nf.iloc[i,2] > 250 or nf.iloc[i,0] < 18 ) ], : ].index
     )
#nf = nf.drop( nf[ (nf.PPGENDER > 2) | (nf.OCCUPATION_col > 28) | (nf.TEACHSTEM > 2) | (nf.SCH4 > 4) ].index )
nf.reset_index(drop=True, inplace=True)
#nf.columns = meta.column_names
print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


###
###  logistic fitting
###
##  load up fitting vectors
##   the features/predictors
#      1 Age: Age in years , linear
#      2 Sex: Sex (0 = male; 1 = female) , nominal
#      3 Height: Height in centimeters , linear
#      4 Weight: Weight in kilograms , linear
#      5 QRS duration: Average of QRS duration in msec., linear
#      6 P-R interval: Average duration between onset of P and Q waves in msec., linear
#      7 Q-T interval: Average duration between onset of Q and offset of T waves in msec., linear
#      8 T interval: Average duration of T wave in msec., linear
#      9 P interval: Average duration of P wave in msec., linear
#     Vector angles in degrees on front plane of:, linear
#     10 QRS
#     11 T
#     12 P
#     13 QRST
#     14 J
#     15 Heart rate: Number of heart beats per minute ,linear
fea1 = 2; x_label = "Height (cm)";
X = np.array( [ np.ones( len(nf) ),
               nf.iloc[:,fea1].to_numpy(),
               nf.iloc[:,fea1].to_numpy()*nf.iloc[:,fea1].to_numpy(),
               ] ).T

##  the response
y = np.array( [ nf.iloc[:,1].to_numpy() ] ).T
pos_response = "female"
neg_response = "male"

m,n = X.shape

##  standardization
Z, X_mu, X_std = standardizer( X )

##  initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T

##  learning loop
beta = gradient_descent(
         y, Z, beta, logistic.hypothesis, logistic.cost,
         alpha = 1.00E-1, tol = 1.0E-07, max_iter =  30000, report =  3000 )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std )

##
##  plotting
##
x1 = np.linspace( np.amin(X[:,1],axis=0), np.amax(X[:,1],axis=0), num=8192 )
z1 = np.linspace( np.amin(Z[:,1],axis=0), np.amax(Z[:,1],axis=0), num=8192 )
fig, (Xplt,Zplt) = plt.subplots(1, 2, sharey=True, tight_layout=True)

#  first subplot
Xplt.plot( x1, logistic.sigmoid( theta[2]*x1*x1 + x1*theta[1] + theta[0] ), 'b')
#Xplt.plot( x1, logistic.sigmoid( x1*theta[1] + theta[0] ), 'b')
Xplt.scatter( X[:,1], y.T, color='r')
Xplt.set_title(r"unscaled $\mathbf{X},\theta$ space")
Xplt.set_xlabel(x_label)
Xplt.set_ylabel('response')
Xplt.set_yticks( [0.00,1.00] )
Xplt.set_yticklabels( ['Male: 0','Female: 1'] )

#  second subplot
Zplt.plot( z1, logistic.sigmoid( beta[2]*z1*z1 + z1*beta[1] + beta[0] ), 'b')
#Zplt.plot( z1, logistic.sigmoid( z1*beta[1] + beta[0] ), 'b')
Zplt.scatter( Z[:,1], y.T, color='r')
Zplt.set_title(r"standardized $\mathbf{Z},\beta$ fitting space")

#plt.legend()
plt.show()
#plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, facecolor=fig.get_facecolor(), edgecolor="None", metadata=None)

