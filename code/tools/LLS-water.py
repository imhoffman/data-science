import matplotlib.pyplot as plt
import numpy as np
from bclearn import lls
from bclearn.general import *

## http://web.cecs.pdx.edu/~gerry/MATLAB/plotting/loadingPlotData.html#loadAndPlot
target_url = 'http://web.cecs.pdx.edu/~gerry/MATLAB/plotting/examples/PDXprecip.dat'
stuff = np.genfromtxt( target_url, delimiter='\t', skip_header=0 )

#target_url = 'http://www.ncl.ucar.edu/Applications/Data/asc/asc2.txt'
#stuff = np.genfromtxt( target_url, delimiter='\t', skip_header=1 )

#stuff = np.genfromtxt( "../../data/misc/water.txt", delimiter='\t', skip_header=0 )

###
###  data selection and conditioning
###
#nf = stuff.dropna()
nf = stuff
#nf = nf.astype(int, errors="ignore")
#nf = nf.apply(pd.to_numeric, errors="ignore")
#nf = nf.drop(
#    nf.iloc[ [ i for i in range(len(nf))
#        if ( nf.iloc[i,0] != "M" and nf.iloc[i,0] != "F"
#              or nf.iloc[i,3] > 0.40 ) ], : ].index )
#             nf.iloc[i,0] > 18 or
#             nf.iloc[i,3] > 150
#             or nf.iloc[i,6] == "?" ) ], : ].index
#     ) 
#nf = nf.apply(pd.to_numeric, errors="ignore")
#nf.reset_index(drop=True, inplace=True)
#print("\n dropped %d rows during selection\n\n" % ( len(stuff)-len(nf) ) )


##  load up fitting vectors
##   the features/predictors
x_label = "month of the year"
X = np.array( [ np.ones( len(nf) ),
               nf[:,0],
               np.multiply( nf[:,0], nf[:,0] ),
               ] ).T
boundary = lls.quadratic_boundary
#boundary = lls.linear_boundary

##  the response
y_label = "1961$-$1990 average precipitation at PDX (inches)"
y = np.array( nf[:,1], ndmin=2 ).T

m,n = X.shape

##  standardization
Z, X_mu, X_std, yZ, y_mu, y_std = standardizer( X, y )

##  learning loop
##   initialize beta vector of features for Z space
beta = np.array( [ np.ones(n) ], ndmin=2 ).T
#beta = np.array( [ np.zeros(n) ], ndmin=2 ).T
beta = gradient_descent(
         yZ, Z, beta, lls.hypothesis, lls.cost,
         alpha = 3.00E-3, tol = 1.0E-07,
         max_iter= 300000, report= 10000 )

#beta = normal_equations( yZ, Z )
theta2 = normal_equations( y, X )

##  unscale: map from Z--beta space back to X--theta space
theta = unstandardizer( beta, X_mu, X_std, y_mu, y_std )

##
##  plotting
##

x1 = np.linspace( np.amin(X[:,1],axis=0), np.amax(X[:,1],axis=0), num=8192 )
#z1 = np.linspace( np.amin(Z[:,1],axis=0), np.amax(Z[:,1],axis=0), num=8192 )
#fig, (Xplt,Zplt) = plt.subplots(1, 2, sharey=False, tight_layout=True)
fig, Xplt = plt.subplots(1, 1, sharey=False, tight_layout=True)

#  first subplot
Xplt.plot( x1, boundary( x1, theta ), 'b-',
     label="$\mathbf{Z},\\beta$ fit\n$R^2$=%g\n$\chi^2$=%g\n$\chi^2_\\nu$=%g" % 
          ( R_squared(X, theta, y),
            chi_squared( X, theta, y ),
            reduced_chi_squared( X, theta, y ) ) )
Xplt.plot( x1, boundary( x1, theta2 ), 'm--',
     label="$\mathbf{X},\\theta$ inversion\n$R^2$=%g\n$\chi^2$=%g\n$\chi^2_\\nu$=%g" % 
          ( R_squared(X, theta2, y),
            chi_squared( X, theta2, y ),
            reduced_chi_squared( X, theta2, y ) ) )
Xplt.scatter( X[:,1], y, color='r', label=None )
Xplt.set_xlim( [ np.amin(X[:,1]), np.amax(X[:,1]) ] )
Xplt.set_ylim( [ np.amin(y), np.amax(y) ] )
Xplt.set_title(r"unscaled $\mathbf{X},\theta$ space")
Xplt.set_xlabel(x_label)
Xplt.set_ylabel(y_label)

#  second subplot
#Zplt.scatter( Z[:,1], yZ, color='r' )
#Zplt.set_xlim( [ np.amin(Z[:,1]), np.amax(Z[:,1]) ] )
#Zplt.set_ylim( [ np.amin(yZ), np.amax(yZ) ] )
#Zplt.set_title(r"standardized $\mathbf{Z},\beta$ fitting space")
#
#Xplt.set(aspect=1)
#Zplt.set(aspect=1)
Xplt.legend(loc=0)
#Zplt.legend(loc=4)
#plt.show()
plt.savefig("out.png", dpi="figure", papertype="letter", format="png", transparent=False, bbox_inches=None, pad_inches=0.1, facecolor=fig.get_facecolor(), edgecolor="None", metadata=None)

