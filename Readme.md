Messing around in "data science" with interesting sets, as I find them (and the time).
* At present, I am working on rolling my own basic [tools](code/tools) to replace sklearn; my hope is to not need to import anything for my analyses besides my own code (as well as numpy, plotting, and a bit of pandas for parsing proprietary data formats).
* I have already rolled my own logistic fits in matlab for [my seismograph analysis](https://gitlab.com/imhoffman/shake)&mdash;I am now revisiting it [here](code/shake).
* [The Pew 2017 STEM survey](https://www.pewresearch.org/science/datasets/) is planned to be analyzed [here](code/2017-STEM) eventually.
* [The Pew 2014 Views of the Future](https://www.pewresearch.org/science/datasets/) was my guinea pig for learning [R on my github](https://github.com/imhoffman/R/tree/master/Pew) and is my ultimate target for analysis here.
